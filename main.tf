terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      # Root module should specify the maximum provider version
      # The ~> operator is a convenient shorthand for allowing only patch releases within a specific minor release.
      version = "~> 2.26"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_storage_account" "storage_account" {
  name = "${var.storageaccount}"
  resource_group_name = "${var.resourcegroup}"
  location = "${var.location}"
  account_tier = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "app_service" {
  name                = "${var.appservice}"
  location            = "${var.location}"
  resource_group_name = "${var.resourcegroup}"

  sku {
    tier = "Basic"
    size = "B1"
  }
}

resource "azurerm_function_app" "function_app" {
  name                       = "${var.functionappchunk}"
  resource_group_name        = "${var.resourcegroup}"
  location                   = "${var.location}"
  app_service_plan_id        = azurerm_app_service_plan.app_service.id
  os_type = "linux"
  storage_account_name       = azurerm_storage_account.storage_account.name
  storage_account_access_key = azurerm_storage_account.storage_account.primary_access_key
  version                    = "~3"
}

resource "azurerm_search_service" "search" {
  name                = "${var.cognitivesearch}"
  resource_group_name = "${var.resourcegroup}"
  location            = "${var.location}"
  sku                 = var.cognitivesearchsku
  replica_count       = var.replicacount
  partition_count     = var.partitioncount
}