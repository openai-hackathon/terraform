variable "storageaccount" {
  type = string
  description = "Storage account name"
  default = "globestotf"
}

variable "location" {
  type = string
  description = "Location name"
  default = "eastus"
}

variable "resourcegroup" {
  type = string
  default = "rgroup-icm"
}

variable "functionappchunk" {
  type = string
  default = "globefunctf"
}

variable "functionapplookup" {
  type = string
  default = "globefunclookuptf"
}

variable "appservice" {
  type = string
  default = "globeicmpoctf"
}

variable "cognitivesearch" {
  type = string
  default = "globecogsetf"
}

variable "cognitivesearchsku" {
  type = string
  default = "standard"
}

variable "replicacount" {
  type = number
  default = 1
}

variable "partitioncount" {
  type = number
  default = 1
}

variable "appserviceid" {
  type = string
  default = "/subscriptions/6e965d3a-9f41-44ec-8936-181378cd7ffe/resourceGroups/rgroup-icm/providers/Microsoft.Web/sites/globeicmpoc"
}